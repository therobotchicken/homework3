#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	xPixels = 800;
	yPixels = 600;
	webCam.setup(xPixels, yPixels);
	frame.allocate(xPixels, yPixels, OF_IMAGE_COLOR);
	cam.setPosition(800, 300, 550);
	light1.setPosition(400, 300, 600);
	ofEnableDepthTest();

	gui.setup();
	gui.add(colorToggle.setup("Color", false));

	for (int x = 0; x < xPixels; x += boxResolution)
	{
		for (int y = 0; y < yPixels; y += boxResolution)
		{
			ofBoxPrimitive box;
			box.set(boxResolution);
			box.setPosition(x, y, 0);
			boxes.push_back(box);
		}
	}
}

//--------------------------------------------------------------
void ofApp::update(){
	if (webCam.isInitialized())
	{
		webCam.update();
		if (webCam.isFrameNew())
		{
			frame.setFromPixels(webCam.getPixels());
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	cam.begin();
	ofEnableLighting();
	light1.enable();
	frame.draw(800, 0);
	for (auto&& box : boxes)
	{
		ofColor c;

		//-------------------------------------------------------------------------------------
		//Attempt to use average hue of 10x10 pixels. Behaved very strangely by either setting or getting a constant hue value
		//c.setHsb(255, 255, 255);	//NEED to setHsb() for some reason to make setHue() work
		////get average hue of 10x10 pixels
		//float hueTotal = 0;
		//for (int x = box.getX(); x < box.getX() + 10; x++)
		//{
		//	for (int y = box.getY(); y < box.getY() + 10; y++)
		//	{
		//		hueTotal =+ frame.getColor(x, y).getHue();

		//	}
		//}
		//float hueAverage = hueTotal / 100.0;
		//hueAverage *= 1.90;	//the average is consistently low; add 70% to compensate
		//c.setHue(hueAverage);
		//-------------------------------------------------------------------------------------

		box.rotateDeg(frame.getColor(box.getX(), box.getY()).getHueAngle(), glm::vec3(1,0,0));	//rotates bos degrees = hue angle
		c.set(frame.getColor(box.getX(), box.getY()));	//get color of pixel
		//color box if toggled on
		if (colorToggle)
		{
			for (int i = 0; i < 6; i++)
			{
				box.setSideColor(i, c);
			}
		}
		//or white if off
		else
		{
			for (int i = 0; i < 6; i++)
			{
				box.setSideColor(i, c.white);
			}
		}
		box.draw();
	}
	light1.disable();
	cam.end();

	gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
