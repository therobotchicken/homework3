#pragma once

#include "ofMain.h"
#include "ofxGui.h"
class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	int xPixels;
	int yPixels;
	const static int boxResolution = 10;	//the number of pixels a box covers, preferably a clean divisor of xPixels*yPixels
	vector<ofBoxPrimitive> boxes;
	ofVideoGrabber webCam;
	ofImage frame;
	ofEasyCam cam;
	ofLight light1;

	ofxPanel gui;
	ofxToggle colorToggle;
};
